package lms.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class LibraryCollection {

	private Map <Integer, Holding> holdings = new LinkedHashMap <Integer, Holding>();
	private String collectionCode;
	private String collectionName;
	
	
	
	
	public LibraryCollection(String collectionCode, String collectionName) {
		this.collectionCode = collectionCode;
		this.collectionName = collectionName;
		
	}
	
	
	public boolean addHolding(Holding holding) {
		
		//if holdings contain holding return false
		
		// this needs to be the else condition
		holdings.put(holding.getCode(),  holding);
		return true;
	}

	

	
public Holding getHolding(int holdingId)
	{
	//if holding is present the return holding else return null
	if (holdings.containsKey(holdingId))
		  return holdings.get(holdingId);
	return null;
		

				
	}

public Map<Integer, Holding> getAllHoldings() {
	
	return holdings;
}

	
public Boolean removeHolding(int holdingId) {
	if (holdings.containsKey(holdingId))
		  holdings.remove(holdingId);
			return true;
		
	}
	
	
public String toString(){
	
	String returnString = 	collectionCode + ":" + collectionName ;
	
	// Appends any existing holdings within system.
	if (holdings.size() != 0)
	{
		returnString +=  ":";
		for (Map.Entry<Integer, Holding> entry : holdings.entrySet())
			returnString += (entry.getValue().getCode()+ ",");
		return returnString.substring(0, returnString.length() -1);
	}
	return returnString;
}


public int countBooks() {
	int bookCount = 0;
	for (Holding holding : holdings.values())
	{
		if (holding instanceof Book) 
			bookCount++;
	}
	 return bookCount;
}


public int countVideos() {
	int VideoCount = 0;
	for (Holding holding : holdings.values())
	{
		if (holding instanceof Video) 
			VideoCount++;
	}
	return VideoCount;
}



	
	
	


}
