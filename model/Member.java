package lms.model;

import java.util.ArrayList;
import java.util.List;

public interface Member extends Borrower{

	void borrowHolding(int holdingId);
}
