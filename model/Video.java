package lms.model;

public class Video extends AbstractHolding {

	//public  static final int DEFAULT_FEE = 4 ;
	public final static int LOAN_PERIOD = 7 ;
	public final static String VIDEO_TEXT = "VIDEO";
	
	/*public Video(int code, String title) {
		super(code, title,  DEFAULT_FEE, LOAN_PERIOD);
		
	} */
	
	public Video(int code, String title, int loanfee) {
		super(code, title,  loanfee, LOAN_PERIOD);
		
		
	}
	

	public String toString(){
		
		return (super.toString() + ":" + VIDEO_TEXT);
				
	}

}
