package lms.model;


import java.util.Collection;
import java.util.Map;


public interface Borrower{


	
	public void borrowHolding(int holdingId);
	public void returnHolding();
	public Map<Integer, Holding> getCurrentHoldings();
	public BorrowingHistory getBorrowingHistory();
}
