package lms.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class StandardMember extends AbstractMember {

	public final static int MAX_CREDIT = 30;
	private static final String STANDARD_TEXT = "STANDARD";
	
	public StandardMember(String standardMemberId, String standardMemberName) {
	
		super(standardMemberId, standardMemberName,  MAX_CREDIT);
		
	}

	public void borrowHolding(){
		
		
	}
	
	public void returnHolding(){
	
		
	}

	public String toString(){
				
		return (super.toString() + ":" + STANDARD_TEXT);
				
	}
	
}
