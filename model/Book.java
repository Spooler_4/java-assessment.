package lms.model;

public class Book extends  AbstractHolding {

	public final static int DEFAULT_FEE = 10 ;
	public final static int LOAN_PERIOD = 28 ;
	public final static String BOOK_TEXT = "BOOK";
			
	public Book(int code, String title) {
		super(code, title, DEFAULT_FEE, LOAN_PERIOD);
	}

	public String toString(){
		
		return (super.toString() + ":" + BOOK_TEXT);
				
	}

	

}
