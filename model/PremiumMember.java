package lms.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class PremiumMember extends AbstractMember {
	
	public final static int MAX_CREDIT = 45;
	private static final String PREMIUM_TEXT = "PREMIUM";
	
	public PremiumMember(String premiumMemberId, String premiumMemberName) {
	
		super(premiumMemberId, premiumMemberName, MAX_CREDIT);
	}
	
	
	public void borrowHolding(){
		
		
	}
	
	public void returnHolding(){
	
		
	
	}

	public String toString(){
		
		return (super.toString() + ":" + PREMIUM_TEXT);
				
	}
	
}
