package lms.model;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Library {

	private Member member;
	private LibraryCollection collection;
	
	

	
	
	public Library(){
		
	}
	
	
	public Library(Member member, LibraryCollection collection){
	
		this.member = member;
		this.collection = collection;
	}
	
	public boolean addHolding(Holding holding) {
		// TODO Auto-generated method stub
		return collection.addHolding(holding);
	}


	public void addMember(Member member) {
		this.member = member;
		
	}


	public void addcollection(LibraryCollection collection) {
		this.collection = collection;
		
	}
	
	public Member getMember(){
		
		return member;
	}
	
	public LibraryCollection getCollection()
	{
		
		return collection;
	}
	
	
	
	public Holding getHolding(int holdingId)
	{
		
		 return collection.getHolding(holdingId);
	}


	public Map<Integer, Holding> getAllHoldings() {
		return collection.getAllHoldings();
		
	}


	public int countBooks() {
		
		return collection.countBooks();
	}


	public int countVideos() {
		// TODO Auto-generated method stub
		return collection.countVideos();
	}


	public boolean removeHolding(int holdingId) {
		// TODO Auto-generated method stub
		return collection.removeHolding(holdingId);
	}


	public void borrowHolding(int holdingId) {
		member.borrowHolding(holdingId);
		System.out.println("borrow initialized");
		
	}


	public Map<Integer,Holding> getBorrowedHoldings() {
		// TODO Auto-generated method stub
		return member.getCurrentHoldings();
	}







}
