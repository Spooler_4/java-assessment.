package lms.model;

public abstract class AbstractHolding implements Holding {

	protected int code;
	protected String title;
	protected int fee;
	protected int maxLoanPeriod;
	protected int loanFee;
	
	
	public AbstractHolding(int code, String title, int fee, int maxLoanPeriod){
	this.code = code;
	this.title = title;
	this.fee = fee;
	this.maxLoanPeriod = maxLoanPeriod;

	}
	
	public String toString(){
		
		
		String returnString = code + ":" + title + ":" + fee + ":" + maxLoanPeriod;
		return returnString;
		
		
		
	}
	
	public int getCode(){
		return code;
	}
	
	public int setloanFee(){
		return this.loanFee = fee;
	}
	
	
	public int getDefaultLoanFee()
	
	{	
		return this.loanFee;
	
	}
}
