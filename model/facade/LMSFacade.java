package lms.model.facade;



import java.util.Collection;
import java.util.Map;

import lms.model.HistoryRecord;
import lms.model.Holding;
import lms.model.Library;
import lms.model.LibraryCollection;
import lms.model.Member;
import lms.model.exception.InsufficientCreditException;
import lms.model.exception.MultipleBorrowingException;
import lms.model.exception.OverdrawnCreditException;

public class LMSFacade implements LMSModel {
	
	private Library library;
	
	public LMSFacade(){
		library = new Library();
	}
	
	public void addMember(Member member) {
	library.addMember(member);
		
	}
	
	public void addCollection(LibraryCollection collection) {
		library.addcollection(collection);
		
	}
	
	public Member getMember() {
		
		return library.getMember();
	}
	
	public LibraryCollection getCollection() {
		
		return library.getCollection();
	}

	public boolean addHolding(Holding holding) {
		return library.addHolding(holding);
		
	}
	
	public boolean removeHolding(int holdingId) {
		
		return library.removeHolding(holdingId);
	}
	
	public Holding getHolding(int holdingId) {
		return library.getHolding(holdingId);
		//return null;
		
	}
	
	public Holding[] getAllHoldings() {
		
		//convert collection to array. within facade
		Map<Integer, Holding> holdings = library.getAllHoldings();
		// return null if there's no holdings
		if (holdings.isEmpty())
			return null;
		return holdings.values().toArray(new Holding[holdings.size()]);
		
		
		
	}
	
	
	
public Holding[] getBorrowedHoldings() {
		
	Map<Integer, Holding> Loans = library.getBorrowedHoldings();
	
	// return null if there's no holdings
	if (Loans.isEmpty())
		return null;
	return Loans.values().toArray(new Holding[Loans.size()]);
	 //stringArray;
	}
	
	
	public int countBooks() {
		// TODO Auto-generated method stub
		return library.countBooks();
	}
	
	public int countVideos() {
		// TODO Auto-generated method stub
		return library.countVideos();
	}
	
	public void borrowHolding(int holdingId)
			throws InsufficientCreditException, MultipleBorrowingException {
		library.borrowHolding(holdingId);
		
		// TODO Auto-generated method stub
		
	}
	
	public void returnHolding(int holdingId) throws OverdrawnCreditException {
		// TODO Auto-generated method stub
		
	}

	public HistoryRecord[] getBorrowingHistory() {
		// TODO Auto-generated method stub
		return null;
	}

	public HistoryRecord getHistoryRecord(int holdingId) {
		// TODO Auto-generated method stub
		return null;
	}

	

	public void resetMemberCredit() {
		// TODO Auto-generated method stub
		
	}

	public int calculateRemainingCredit() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int calculateTotalLateFees() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setDate(String currentDate) {
		// TODO Auto-generated method stub
		
	}
	// ... TO DO ...
	
	
	
	
	
}