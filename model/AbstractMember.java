package lms.model;


import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import lms.model.util.DateUtil;

public abstract class AbstractMember implements Member{

	protected String memberId;
	protected String memberName;
	protected  Map <Integer, Holding> currentLoans = new LinkedHashMap <Integer, Holding>();
	protected BorrowingHistory history;
	protected int maxCredit;
	protected int currentCredit;
//	protected String borrowDate = DateUtil.getInstance().getDate();
//	protected int numOfDays = DateUtil.getInstance().getElapsedDays(borrowDate);
	

	
	public AbstractMember(String memberId, String memberName, int maxCredit){
		this.memberId = memberId;
		this.memberName = memberName;
		this.maxCredit = this.currentCredit = maxCredit;
	}
	
	



	public void borrowHolding(int holdingId) {
		
		
		System.out.println("attempting to borrow");
		currentLoans.put(holdingId, null);
		System.out.println("borrowed");
		
		
	}

	public Map<Integer, Holding> getCurrentHoldings() {
		
		
		System.out.println(currentLoans);
		return ((Map<Integer, Holding>)currentLoans);
	
}
	
	public BorrowingHistory getBorrowingHistory() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public void resetCredit()
	{
		currentCredit = maxCredit;
	}
	
	
	//returnHolding()
	
	public String toString(){
		
		
		String returnString = memberId + ":" + memberName + ":" + currentCredit;
		return returnString;
		
		
		
	}
	
}
